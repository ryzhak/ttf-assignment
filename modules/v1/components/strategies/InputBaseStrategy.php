<?php

namespace app\modules\v1\components\strategies;

use app\modules\v1\components\interfaces\InputStrategyInterface;
use app\modules\v1\components\StrategyRule;
use yii\base\Exception;

class InputBaseStrategy implements InputStrategyInterface
{

    public $rulesX = [];
    public $rulesY = [];

    protected $X = null;
    protected $Y = null;

    function __construct($mInput){

        //rule for X 1
        $this->rulesX[0] = new StrategyRule(function() use ($mInput){
            if($mInput->A && $mInput->B && !$mInput->C) $this->X = self::X_S;
        });

        //rule for X 2
        $this->rulesX[1] = new StrategyRule(function() use ($mInput){
            if($mInput->A && $mInput->B && $mInput->C) $this->X = self::X_R;
        });

        //rule for X 3
        $this->rulesX[2] = new StrategyRule(function() use ($mInput){
            if(!$mInput->A && $mInput->B && $mInput->C) $this->X = self::X_T;
        });

        //rule for Y 1
        $this->rulesY[0] = new StrategyRule(function() use ($mInput){
            if($this->X == self::X_S) $this->Y = $mInput->D + ($mInput->D * $mInput->E / 100);
        });

        //rule for Y 2
        $this->rulesY[1] = new StrategyRule(function() use ($mInput){
            if($this->X == self::X_R) $this->Y = $mInput->D + ($mInput->D * ($mInput->E - $mInput->F) / 100);
        });

        //rule for Y 3
        $this->rulesY[2] = new StrategyRule(function() use ($mInput){
            if($this->X == self::X_T) $this->Y = $mInput->D - ($mInput->D * $mInput->F / 100);
        });

    }

    function output(){

        //executing rules for determining Y
        foreach($this->rulesX as $ruleX){
            $ruleX->run();
        }

        if(!$this->X) throw new Exception('Rules for X not found');

        //executing rules for determining Y
        foreach($this->rulesY as $ruleY){
            $ruleY->run();
        }

        return ['X' => $this->X, 'Y' => $this->Y];

    }

}