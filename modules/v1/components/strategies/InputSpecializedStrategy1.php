<?php

namespace app\modules\v1\components\strategies;

use app\modules\v1\components\StrategyRule;

class InputSpecializedStrategy1 extends InputBaseStrategy
{

    function __construct($mInput){

        parent::__construct($mInput);

        //rewrite rule for Y 2
        $this->rulesY[1] = new StrategyRule(function() use ($mInput){
            if($this->X == self::X_R) $this->Y = 2 * $mInput->D + ($mInput->D * $mInput->E / 100);
        });

    }

}