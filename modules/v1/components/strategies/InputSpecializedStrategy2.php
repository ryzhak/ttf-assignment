<?php

namespace app\modules\v1\components\strategies;

use app\modules\v1\components\StrategyRule;

class InputSpecializedStrategy2 extends InputBaseStrategy
{

    function __construct($mInput){

        parent::__construct($mInput);

        //rewrite rule for X 1
        $this->rulesX[0] = new StrategyRule(function() use ($mInput){
            if($mInput->A && $mInput->B && !$mInput->C) $this->X = self::X_T;
        });

        //add a new rule for X
        $this->rulesX[] = new StrategyRule(function() use ($mInput){
            if($mInput->A && !$mInput->B && $mInput->C) $this->X = self::X_S;
        });

        //rewrite rule for Y 1
        $this->rulesY[0] = new StrategyRule(function() use ($mInput){
            if($this->X == self::X_S) $this->Y = $mInput->F + $mInput->D + ($mInput->D * $mInput->E / 100);
        });

    }

}