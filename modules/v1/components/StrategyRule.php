<?php

namespace app\modules\v1\components;


use yii\base\Exception;

class StrategyRule
{

    private $ruleFunc = null;

    function __construct($func){

        if(!is_callable($func)) throw new Exception('Function should be provided');

        $this->ruleFunc = $func;

    }

    public function run(){
        call_user_func($this->ruleFunc);
    }

}