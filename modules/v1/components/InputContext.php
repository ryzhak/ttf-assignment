<?php

namespace app\modules\v1\components;

use app\modules\v1\components\strategies\InputBaseStrategy;
use app\modules\v1\components\strategies\InputSpecializedStrategy1;
use app\modules\v1\components\strategies\InputSpecializedStrategy2;
use yii\base\Exception;

class InputContext
{

    const MAPPING_BASE = 'base';
    const MAPPING_SPECIALIZED_1 = 'specialized1';
    const MAPPING_SPECIALIZED_2 = 'specialized2';

    private $strategy = null;

    public function __construct($mapping, $mInput) {
        switch ($mapping) {
            case self::MAPPING_BASE:
                $this->strategy = new InputBaseStrategy($mInput);
                break;
            case self::MAPPING_SPECIALIZED_1:
                $this->strategy = new InputSpecializedStrategy1($mInput);
                break;
            case self::MAPPING_SPECIALIZED_2:
                $this->strategy = new InputSpecializedStrategy2($mInput);
                break;
            default: throw new Exception('Mapping not found');
        }
    }

    public function output() {
        return $this->strategy->output();
    }

}