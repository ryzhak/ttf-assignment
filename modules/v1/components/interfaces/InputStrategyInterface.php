<?php

namespace app\modules\v1\components\interfaces;

interface InputStrategyInterface {

    const X_S = 'S';
    const X_R = 'R';
    const X_T = 'T';

    public function output();

}