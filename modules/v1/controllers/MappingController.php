<?php

namespace app\modules\v1\controllers;

use app\modules\v1\components\InputContext;
use app\modules\v1\models\Input;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class MappingController extends \yii\rest\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'base' => ['post'],
                    'specialized1' => ['post'],
                    'specialized2' => ['post'],
                ],
            ],
        ]);
    }

    /*
     * Returns output using base mapping
     */
    public function actionBase(){

        $mInput = new Input(\Yii::$app->request->getBodyParams());

        if($mInput->validate()){

            $inputContext = new InputContext(InputContext::MAPPING_BASE, $mInput);

            return $inputContext->output();

        } else {

            \Yii::$app->response->statusCode = 400;
            return $mInput->errors;

        }

    }

    /*
     * Returns output using the 1st specialized mapping
     */
    public function actionSpecialized1(){

        $mInput = new Input(\Yii::$app->request->getBodyParams());

        if($mInput->validate()){

            $inputContext = new InputContext(InputContext::MAPPING_SPECIALIZED_1, $mInput);

            return $inputContext->output();

        } else {

            \Yii::$app->response->statusCode = 400;
            return $mInput->errors;

        }

    }

    /*
     * Returns output using the 2nd specialized mapping
     */
    public function actionSpecialized2(){

        $mInput = new Input(\Yii::$app->request->getBodyParams());

        if($mInput->validate()){

            $inputContext = new InputContext(InputContext::MAPPING_SPECIALIZED_2, $mInput);

            return $inputContext->output();

        } else {

            \Yii::$app->response->statusCode = 400;
            return $mInput->errors;

        }

    }

}