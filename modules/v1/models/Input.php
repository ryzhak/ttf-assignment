<?php

namespace app\modules\v1\models;

use Yii;
use yii\base\Model;

class Input extends Model
{
    public $A;
    public $B;
    public $C;
    public $D;
    public $E;
    public $F;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['A', 'B', 'C', 'D', 'E', 'F'], 'required'],
            [['A', 'B', 'C'], 'boolean'],
            [['D', 'E', 'F'], 'integer']
        ];
    }

}