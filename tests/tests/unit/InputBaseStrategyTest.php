<?php

use app\modules\v1\components\strategies\InputBaseStrategy;
use app\modules\v1\models\Input;

class InputBaseStrategyTest extends \Codeception\TestCase\Test
{
    protected $inputBaseStrategy;
    protected $mInput;
    protected $tester;

    /*
     * Tests
     */
    public function testConstruct_OnCreate_RulesAreNotEmpty()
    {
        $this->mInput = new Input();
        $this->inputBaseStrategy = new InputBaseStrategy($this->mInput);
        $this->tester->assertNotNull($this->inputBaseStrategy->rulesX);
        $this->tester->assertNotNull($this->inputBaseStrategy->rulesY);
    }

    public function testOutput_OnRuleX1Y1_ReturnsCorrectXAndYValues(){
        $this->mInput = new Input([
            'A' => 1,
            'B' => 1,
            'C' => 0,
            'D' => 1,
            'E' => 2,
            'F' => 3
        ]);
        $this->inputBaseStrategy = new InputBaseStrategy($this->mInput);
        $output = $this->inputBaseStrategy->output();
        $this->tester->assertEquals($output['X'], InputBaseStrategy::X_S);
        $this->tester->assertEquals($output['Y'], 1.02);
    }

    public function testOutput_OnRuleX2Y2_ReturnsCorrectXAndYValues(){
        $this->mInput = new Input([
            'A' => 1,
            'B' => 1,
            'C' => 1,
            'D' => 1,
            'E' => 2,
            'F' => 3
        ]);
        $this->inputBaseStrategy = new InputBaseStrategy($this->mInput);
        $output = $this->inputBaseStrategy->output();
        $this->tester->assertEquals($output['X'], InputBaseStrategy::X_R);
        $this->tester->assertEquals($output['Y'], 0.99);
    }

    public function testOutput_OnRuleX3Y3_ReturnsCorrectXAndYValues(){
        $this->mInput = new Input([
            'A' => 0,
            'B' => 1,
            'C' => 1,
            'D' => 1,
            'E' => 2,
            'F' => 3
        ]);
        $this->inputBaseStrategy = new InputBaseStrategy($this->mInput);
        $output = $this->inputBaseStrategy->output();
        $this->tester->assertEquals($output['X'], InputBaseStrategy::X_T);
        $this->tester->assertEquals($output['Y'], 0.97);
    }

}