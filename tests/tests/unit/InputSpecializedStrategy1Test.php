<?php

use app\modules\v1\components\strategies\InputBaseStrategy;
use app\modules\v1\components\strategies\InputSpecializedStrategy1;
use app\modules\v1\models\Input;

class InputSpecializedStrategy1Test extends \Codeception\TestCase\Test
{

    protected $inputSpecializedStrategy1;
    protected $mInput;
    protected $tester;

    /*
    * Tests
    */
    public function testOutput_OnSpecializedRule_ReturnsCorrectXAndYValues(){
        $this->mInput = new Input([
            'A' => 1,
            'B' => 1,
            'C' => 1,
            'D' => 1,
            'E' => 2,
            'F' => 3
        ]);
        $this->$inputSpecializedStrategy1 = new InputSpecializedStrategy1($this->mInput);
        $output = $this->inputBaseStrategy->output();
        $this->tester->assertEquals($output['X'], InputBaseStrategy::X_R);
        $this->tester->assertEquals($output['Y'], 2.02);
    }

}