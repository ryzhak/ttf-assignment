<?php

use app\modules\v1\components\strategies\InputBaseStrategy;
use app\modules\v1\components\strategies\InputSpecializedStrategy2;
use app\modules\v1\models\Input;

class InputSpecializedStrategy2Test extends \Codeception\TestCase\Test
{

    protected $inputSpecializedStrategy2;
    protected $mInput;
    protected $tester;

    /*
    * Tests
    */
    public function testOutput_OnSpecializedRuleForX1_ReturnsCorrectXAndYValues(){
        $this->mInput = new Input([
            'A' => 1,
            'B' => 1,
            'C' => 0,
            'D' => 1,
            'E' => 2,
            'F' => 3
        ]);
        $this->$inputSpecializedStrategy2= new InputSpecializedStrategy2($this->mInput);
        $output = $this->inputBaseStrategy->output();
        $this->tester->assertEquals($output['X'], InputBaseStrategy::X_T);
        $this->tester->assertEquals($output['Y'], 0.97);
    }

    public function testOutput_OnNewRuleForXAndUpdatedY1_ReturnsCorrectXAndYValues(){
        $this->mInput = new Input([
            'A' => 1,
            'B' => 0,
            'C' => 1,
            'D' => 1,
            'E' => 2,
            'F' => 3
        ]);
        $this->$inputSpecializedStrategy2= new InputSpecializedStrategy2($this->mInput);
        $output = $this->inputBaseStrategy->output();
        $this->tester->assertEquals($output['X'], InputBaseStrategy::X_S);
        $this->tester->assertEquals($output['Y'], 4.02);
    }

}