<?php

/**
 * @apiDefine InputAttributes
 * @apiParam {Integer} A Param A. Can be 0 or 1.
 * @apiParam {Integer} B Param B. Can be 0 or 1.
 * @apiParam {Integer} C Param C. Can be 0 or 1.
 * @apiParam {Integer} D Param D.
 * @apiParam {Integer} E Param E.
 * @apiParam {Integer} F Param F.
 */

/**
 * @api {post} /v1/mapping/base Get base output
 * @apiVersion 0.1.0
 * @apiName InputBase
 * @apiGroup Input
 *
 * @apiDescription Returns output using base mapping.
 *
 * @apiUse InputAttributes
 *
 * @apiExample Example:
 * {"A":1,"B":1,"C":1,"D":1,"E":2,"F":3}
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {"X":"R","Y":0.99}
 *
 */

/**
 * @api {post} /v1/mapping/specialized1 Get specialized1 output
 * @apiVersion 0.1.0
 * @apiName InputSpecialized1
 * @apiGroup Input
 *
 * @apiDescription Returns output using specialized1 mapping.
 *
 * @apiUse InputAttributes
 *
 * @apiExample Example:
 * {"A":1,"B":1,"C":1,"D":1,"E":2,"F":3}
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {"X":"R","Y":2.02}
 *
 */

/**
 * @api {post} /v1/mapping/specialized2 Get specialized2 output
 * @apiVersion 0.1.0
 * @apiName InputSpecialized2
 * @apiGroup Input
 *
 * @apiDescription Returns output using specialized2 mapping.
 *
 * @apiUse InputAttributes
 *
 * @apiExample Example:
 * {"A":1,"B":1,"C":0,"D":1,"E":2,"F":3}
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {"X":"T","Y":0.97}
 *
 */
