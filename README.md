Test assignment project for TidTilForsikring
============================
The source text of the assignment can be found [here](http://tidtilforsikring.dk/jobs/assignment.pdf).

[API documentation](http://www.ryzhak.com/public/ttf-assignment/web/docs/)

#### How to run the application
-------------------
```
cd <PROJECT_ROOT>
composer install
./yii serve
```

#### How to run unit tests
```
cd <PROJECT_ROOT>/tests
codecept run unit
```
