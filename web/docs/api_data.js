define({ "api": [
  {
    "type": "post",
    "url": "/v1/mapping/base",
    "title": "Get base output",
    "version": "0.1.0",
    "name": "InputBase",
    "group": "Input",
    "description": "<p>Returns output using base mapping.</p> ",
    "examples": [
      {
        "title": "Example:",
        "content": "{\"A\":1,\"B\":1,\"C\":1,\"D\":1,\"E\":2,\"F\":3}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"X\":\"R\",\"Y\":0.99}",
          "type": "json"
        }
      ]
    },
    "filename": "tests/tests/functional/MappingControllerCept.php",
    "groupTitle": "Input",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "A",
            "description": "<p>Param A. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "B",
            "description": "<p>Param B. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "C",
            "description": "<p>Param C. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "D",
            "description": "<p>Param D.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "E",
            "description": "<p>Param E.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "F",
            "description": "<p>Param F.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/v1/mapping/specialized1",
    "title": "Get specialized1 output",
    "version": "0.1.0",
    "name": "InputSpecialized1",
    "group": "Input",
    "description": "<p>Returns output using specialized1 mapping.</p> ",
    "examples": [
      {
        "title": "Example:",
        "content": "{\"A\":1,\"B\":1,\"C\":1,\"D\":1,\"E\":2,\"F\":3}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"X\":\"R\",\"Y\":2.02}",
          "type": "json"
        }
      ]
    },
    "filename": "tests/tests/functional/MappingControllerCept.php",
    "groupTitle": "Input",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "A",
            "description": "<p>Param A. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "B",
            "description": "<p>Param B. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "C",
            "description": "<p>Param C. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "D",
            "description": "<p>Param D.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "E",
            "description": "<p>Param E.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "F",
            "description": "<p>Param F.</p> "
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/v1/mapping/specialized2",
    "title": "Get specialized2 output",
    "version": "0.1.0",
    "name": "InputSpecialized2",
    "group": "Input",
    "description": "<p>Returns output using specialized2 mapping.</p> ",
    "examples": [
      {
        "title": "Example:",
        "content": "{\"A\":1,\"B\":1,\"C\":0,\"D\":1,\"E\":2,\"F\":3}",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\"X\":\"T\",\"Y\":0.97}",
          "type": "json"
        }
      ]
    },
    "filename": "tests/tests/functional/MappingControllerCept.php",
    "groupTitle": "Input",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "A",
            "description": "<p>Param A. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "B",
            "description": "<p>Param B. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "C",
            "description": "<p>Param C. Can be 0 or 1.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "D",
            "description": "<p>Param D.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "E",
            "description": "<p>Param E.</p> "
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "F",
            "description": "<p>Param F.</p> "
          }
        ]
      }
    }
  }
] });